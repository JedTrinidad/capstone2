package com.zuitt.carrental.repositories;

import com.zuitt.carrental.models.Result;
import org.springframework.data.repository.CrudRepository;

public interface ResultRepository extends CrudRepository<Result, Integer> {
}
