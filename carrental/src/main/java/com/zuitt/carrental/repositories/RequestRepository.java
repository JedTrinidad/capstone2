package com.zuitt.carrental.repositories;

import com.zuitt.carrental.models.Request;
import org.springframework.data.repository.CrudRepository;

public interface RequestRepository extends CrudRepository<Request, Integer> {
}
