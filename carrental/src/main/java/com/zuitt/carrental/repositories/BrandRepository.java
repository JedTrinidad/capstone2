package com.zuitt.carrental.repositories;

import com.zuitt.carrental.models.Brand;
import org.springframework.data.repository.CrudRepository;

public interface BrandRepository extends CrudRepository<Brand, Integer> {
}
