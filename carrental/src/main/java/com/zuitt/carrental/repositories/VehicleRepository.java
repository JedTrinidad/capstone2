package com.zuitt.carrental.repositories;

import com.zuitt.carrental.models.Vehicle;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface VehicleRepository extends CrudRepository<Vehicle, Integer> {
    List<Vehicle> findAllByOrderByNameAsc();
    List<Vehicle> findAllByOrderByNameDesc();

    public static Vehicle a = new Vehicle(
            "Toyota",
            999
    );

    public static List<Vehicle> allVehicles = new ArrayList<Vehicle>() {{
        add(a);
    }};

    public static Vehicle deleteByName(String name) {
        Vehicle temp = null;
        for(int i = 0; i< allVehicles.size(); i++) {
            if(allVehicles.get(i).getName().equals(name)) {
                temp = allVehicles.get(i);
                allVehicles.remove(i);
            }
        }
        return temp;
    }

//    public static void deleteTask(UUID id) {
//        for(int i = 0; i<allTasks.size(); i++) {
//            if(allTasks.get(i).getId().equals(id)) {
//                allTasks.remove(i);
//            }
//        }
//    }
//
//    public void updateTask(UUID id, String status) {
//        Task temp = null;
//        for(int i=0; i<allTasks.size(); i++) {
//            if(allTasks.get(i).getId().equals(id)) {
//                temp = allTasks.get(i);
//                temp.setStatus(status);
//                allTasks.get(i).setStatus(status);
//            }
//        }
//    }
//
//    public void editTask(UUID id, String name) {
//        Task temp = null;
//        for(int i=0; i<allTasks.size(); i++) {
//            if(allTasks.get(i).getId().equals(id)) {
//                temp = allTasks.get(i);
//                temp.setName(name);
//                allTasks.get(i).setName(name);
//            }
//        }
//    }



}
