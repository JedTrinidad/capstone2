package com.zuitt.carrental.repositories;

import com.zuitt.carrental.models.Status;
import org.springframework.data.repository.CrudRepository;


public interface StatusRepository extends CrudRepository<Status, Integer> {
}
