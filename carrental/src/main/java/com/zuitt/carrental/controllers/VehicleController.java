package com.zuitt.carrental.controllers;

import com.zuitt.carrental.models.Brand;
import com.zuitt.carrental.models.Vehicle;
import com.zuitt.carrental.repositories.BrandRepository;
import com.zuitt.carrental.repositories.VehicleRepository;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/vehicles")
@CrossOrigin(origins = "http://localhost:3000")
public class VehicleController {

    @Autowired
    VehicleRepository vehicleRepository;

    @Autowired
    BrandRepository brandRepository;

    @GetMapping("/")
    public Iterable<Vehicle> getVehicles() {
        return vehicleRepository.findAll();
    }

    @GetMapping("/sort/asc")
    public List<Vehicle> getVehiclesAsc() {
        return vehicleRepository.findAllByOrderByNameAsc();
    }

    @GetMapping("/sort/desc")
    public List<Vehicle> getVehiclesDesc() {
        return vehicleRepository.findAllByOrderByNameDesc();
    }

    @GetMapping("/{id}")
    public Vehicle getVehicleById(@PathVariable Integer id) {
        return vehicleRepository.findById(id).get();
    }

    @PostMapping("/{brand_id}")
    public Vehicle addVehicle(@RequestBody Vehicle vehicle, @PathVariable Integer brand_id) {
//        System.out.println(product.getBrand_id());
        Brand brand = brandRepository.findById(brand_id).get();
        vehicle.setBrand(brand);
        return vehicleRepository.save(vehicle);
    }


//    @GetMapping(value = "/images/{image}",
//            produces = MediaType.IMAGE_JPEG_VALUE
//    )
//    public byte[] getImage(@PathVariable String image) throws IOException {
//        System.out.println(image);
//        InputStream in = getClass()
//                .getResourceAsStream("/resources/images/"+image);
//        return IOUtils.toByteArray(in);
//    }

    private static String UPLOADED_FOLDER = "src/main/resources/static/images/";

    @PostMapping("/upload/{veh_id}")
    public String singleFileUpload(@PathVariable Integer veh_id, @RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes) {

        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "error. no image uploaded.";
        }

        try {
            System.out.println(file.getOriginalFilename());
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            Vehicle vehicle = vehicleRepository.findById(veh_id).get();
            vehicle.setImage(file.getOriginalFilename());
            vehicleRepository.save(vehicle);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return file.getOriginalFilename();
    }

    @DeleteMapping("/delete/{id}")
    public void deleteByVehicle(@PathVariable Integer id){
        vehicleRepository.deleteById(id);
    }

//    @PutMapping("/edit/{vehicle_id}")
//    public Vehicle updateVehicle(@RequestBody Vehicle vehicle, @PathVariable Integer vehicles_id){
//        Vehicle vehicle = vehicleRepository.findById(vehicle_id);
//        vehicle.setName(vehicle);
//        return vehicle;
//    }
//
//    @PutMapping("/vehicles/update/{id}")
//    public void updateVehicles(@PathVariable Integer vehicles_id, @RequestBody Vehicle vehicle) {
//        vehicleRepository.updateVehicles(vehicles_id, vehicle);
//    }
//
//    @PutMapping("/vehicles/edit/{id}")
//    public void editVehicles(@PathVariable Integer vehicles_id, @RequestBody Vehicle vehicle) {
//        vehicleRepository.editVehicles(vehicles_id, vehicle);
//    }

}
