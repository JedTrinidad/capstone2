package com.zuitt.carrental.controllers;

import com.zuitt.carrental.models.Brand;
import com.zuitt.carrental.repositories.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/brands")
@CrossOrigin(origins = "http://localhost:3000")
public class BrandController {

    @Autowired
    BrandRepository brandRepository;

    @GetMapping("/")
    public Iterable<Brand> getBrands() {
        return brandRepository.findAll();
    }

    @GetMapping("/{id}")
    public Brand getBrandById(@PathVariable Integer id) {
        return brandRepository.findById(id).get();
    }

    @PostMapping("/")
    public Brand addBrand(@RequestBody Brand category) {
        return brandRepository.save(category);
    }


}
