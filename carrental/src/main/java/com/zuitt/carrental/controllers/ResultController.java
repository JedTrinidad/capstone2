package com.zuitt.carrental.controllers;

import com.zuitt.carrental.models.Result;
import com.zuitt.carrental.repositories.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/results")
@CrossOrigin(origins = "http://localhost:3000")
public class ResultController {

    @Autowired
    ResultRepository resultRepository;

    @GetMapping("/")
    public Iterable<Result> getResults() {
        return resultRepository.findAll();
    }

    @GetMapping("/{id}")
    public Result getResultById(@PathVariable Integer id) {
        return resultRepository.findById(id).get();
    }

    @PostMapping("/")
    public Result addResult(@RequestBody Result result) {
        return resultRepository.save(result);
    }

}
