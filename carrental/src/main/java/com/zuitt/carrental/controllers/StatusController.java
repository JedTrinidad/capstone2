package com.zuitt.carrental.controllers;

import com.zuitt.carrental.models.Status;
import com.zuitt.carrental.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/status")
@CrossOrigin(origins = "http://localhost:3000")
public class StatusController {

    @Autowired
    StatusRepository statusRepository;

    @GetMapping("/")
    public Iterable<Status> getStatus() {
        return statusRepository.findAll();
    }

    @GetMapping("/{id}")
    public Status getStatusById(@PathVariable Integer id) {
        return statusRepository.findById(id).get();
    }

    @PostMapping("/")
    public Status addStatus(@RequestBody Status status) {
        return statusRepository.save(status);
    }


}
