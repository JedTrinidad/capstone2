package com.zuitt.carrental.controllers;

import com.zuitt.carrental.models.Request;
import com.zuitt.carrental.repositories.RequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/requests")
@CrossOrigin(origins = "http://localhost:3000")
public class RequestController {

    @Autowired
    RequestRepository requestRepository;

    @GetMapping("/")
    public Iterable<Request> getRequests() {
        return requestRepository.findAll();
    }

    @GetMapping("/{id}")
    public Request getRequestById(@PathVariable Integer id) {
        return requestRepository.findById(id).get();
    }

    @PostMapping("/")
    public Request addRequest(@RequestBody Request request) {
        return requestRepository.save(request);
    }

}
