package com.zuitt.carrental.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "status")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date approval_date;

//    @OneToMany(mappedBy = "status", cascade = CascadeType.ALL)
//    @JsonIgnore
//    private List<Result> results;
//
//    @ManyToOne
//    @JoinColumn(name = "result_id")
//    @JoinColumn(name = "request_id")
//    private Result result;
//    private Request request;

    public Status() {}

    public Status(int id, Date approval_date) {
        this.id = id;
        this.approval_date = approval_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getApproval_date() {
        return approval_date;
    }

    public void setApproval_date(Date approval_date) {
        this.approval_date = approval_date;
    }




}
