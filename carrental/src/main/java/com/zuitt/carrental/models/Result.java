package com.zuitt.carrental.models;

import javax.persistence.*;

@Entity
@Table(name = "results")
public class Result {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String result_type;

//    @ManyToOne
//    @JoinColumn(name = "status_id")
//    private String status;

    public Result() {}

    public Result(int id, String result_type) {
        this.id = id;
        this.result_type = result_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResult_type() {
        return result_type;
    }

    public void setResult_type(String result_type) {
        this.result_type = result_type;
    }


}
